package com.restaurant.myRest.repositories;

import com.restaurant.myRest.entities.Food;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.UUID;

public interface FoodRepository extends MongoRepository<Food, String> {

}
