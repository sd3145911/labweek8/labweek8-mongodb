package com.restaurant.myRest.services;

import com.restaurant.myRest.dtos.FoodResponseDTO;
import com.restaurant.myRest.dtos.RequestFoodDTO;
import com.restaurant.myRest.entities.Food;
import com.restaurant.myRest.factory.FoodFactory;
import com.restaurant.myRest.repositories.FoodRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FoodService {
    private final FoodRepository foodRepository;

    public FoodService(FoodRepository foodRepository) {
        this.foodRepository = foodRepository;
    }

    public FoodResponseDTO crateFood(RequestFoodDTO dto){
        Food food = new Food();
        BeanUtils.copyProperties(dto, food);
        return FoodFactory.create(foodRepository.save(food));
    }

    public FoodResponseDTO findById(String id){
        Optional<Food> food = foodRepository.findById(id);

        return FoodFactory.create(food.get());
    }
}
