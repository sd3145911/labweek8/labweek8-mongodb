package com.restaurant.myRest.dtos;

import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;


import java.io.Serializable;

@Getter
@Setter
public class FoodResponseDTO extends RepresentationModel<FoodResponseDTO> implements Serializable {
    private String id;
    private String foodName;
    private double price;
    private int amount;


}
