package com.restaurant.myRest.factory;

import com.restaurant.myRest.dtos.FoodResponseDTO;
import com.restaurant.myRest.entities.Food;
import org.springframework.beans.BeanUtils;

public class FoodFactory {
    public static FoodResponseDTO create(Food food){
        FoodResponseDTO foodResponseDTO = new FoodResponseDTO();

        BeanUtils.copyProperties(food, foodResponseDTO);

        return foodResponseDTO;
    }
}
