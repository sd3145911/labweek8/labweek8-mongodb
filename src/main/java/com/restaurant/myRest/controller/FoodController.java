package com.restaurant.myRest.controller;

import com.restaurant.myRest.dtos.FoodResponseDTO;
import com.restaurant.myRest.dtos.RequestFoodDTO;
import com.restaurant.myRest.services.FoodService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Controller
@RequestMapping("/food")
public class FoodController {
    private final FoodService foodService;

    public FoodController(FoodService foodService) {
        this.foodService = foodService;
    }

    @PostMapping("/create")
    public ResponseEntity<FoodResponseDTO> createFood(@Valid @RequestBody RequestFoodDTO requestFoodDTO){
        FoodResponseDTO foodResponse = foodService.crateFood(requestFoodDTO);

        foodResponse.add(linkTo(methodOn(FoodController.class).getFoodByID(foodResponse.getId())).withSelfRel());

        return ResponseEntity.status(HttpStatus.CREATED).body(foodResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<FoodResponseDTO> getFoodByID(@PathVariable String id){
        FoodResponseDTO responseDTO = foodService.findById(id);

        return responseDTO == null ? ResponseEntity.status(HttpStatus.NOT_FOUND).build() : ResponseEntity.status(HttpStatus.OK).body(responseDTO);
    }
}
