package com.restaurant.myRest.entities;
import lombok.Data;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.UUID;


@Data
@Document("foods")
public class Food {
    @Field(name = "_id") @MongoId
    private String id;
    @Field(name = "nm_foods")
    private String foodName;
    @Field(name = "vl_price")
    private double price;
    @Field(name = "qt_amount")
    private int amount;

    public Food(){
        this.id = UUID.randomUUID().toString();
    }
}
